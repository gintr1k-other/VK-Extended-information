var
	profile = false;
	dialogs_page = false;
	dialog = false;

var client_url = window.location.href.split("vk.com")[1];

if (document.getElementsByClassName("profile_online_lv")[0] &&
	client_url.indexOf("/im") == -1 && client_url.indexOf("/im?sel") == -1) {
	profile = true;
	dialogs_page = false;
	dialog = false;
}
else if (client_url == "/im" || client_url == "/im?sel" ||
client_url == "/im?" || client_url == "/im?sel=") {
	profile = false;
	dialogs_page = true;
	dialog = false;
}
else if (client_url.indexOf("/im?sel=") > -1) {
	profile = false;
	dialogs_page = false;
	dialog = true;
}

if (profile || dialogs_page || dialog) {
	if (profile) {
		var
			client = client_url.replace("/id", "").replace("/", ""),
			user_id = window.location.pathname.split('/')[1],

			setUserTime_div = '.profile_online_lv';
	}
	else if (dialog) {
		var
			user_id = client_url.split('/im?sel=')[1],
			setUserTime_div = '._im_page_peer_online',

			online_status_span = $('span._im_last_act')[0];

		if (online_status_span) {
			online_status_span.innerText = "offline";
		}
	}

	Ext = new Extension(user_id, function(ext) {
		ext.setUserTime(setUserTime_div);
		ext.setUserDate();
		ext.setUserID();
	});
}

function Extension(user, callback) {

	this.elements = {
		id: 'vk_user_id',
		time: 'vk_online_time',
		date: 'vk_user_date'
	};

	this.vk_api_url = 'https://api.vk.com/method/users.get';
	this.parameters = {
		user_ids: user,
		fields: 'last_seen',
		v: '5.63'
	};

	var that = this;
	chrome.storage.local.get('settings', function(result) {
		if (result.settings != null) {
			that.settings = result.settings;
		} else {
			that.settings = {};
		}

		$.ajax({
			url: that.vk_api_url,
			type: "POST",
			crossDomain: true,
			cache: false,
			data: that.parameters,
			success: function(vk_api_response) {
				var
					user_info = vk_api_response.response[0];

				$.ajax({
					url: 'https://vk.com/foaf.php?id='+user_info.id,
					type: "GET",
					crossDomain: true,
					cache: false,
					success: function(xml) {
						var
							xmlDoc = $.parseXML(xml),
							$xml 	 = $(xmlDoc),
							$date  = $xml.find("ya\\:created").attr("dc:date");

						user_info.date = $date;
						that.user_info = user_info;

						callback(that);
					}
				});
			}
		});
	});
}

Extension.prototype.setUserID = function() {
	var
		id_div = document.getElementById(this.elements.id);

	if (!id_div && this.settings.user_id != null && this.settings.user_id == true) {
		$(".profile_info.profile_info_short").prepend("<div id=\"vk_user_id\" class=\"clear_fix profile_info_row \"><div class=\"label fl_l\">ID пользователя:</div><div class=\"labeled\">"+this.user_info.id+"</div></div>");
	}
};

Extension.prototype.setUserDate = function() {
	var
		date_div = document.getElementById(this.elements.date),

		date = new Date(this.user_info.date),
		dd 	 = date.getDate(),
		mm 	 = date.getMonth()+1,
		yyyy = date.getFullYear();
		if (dd < 10) {
			dd = '0' + dd;
		}
		if (mm < 10) {
			mm = '0' + mm;
		}
		date = dd + '.' + mm + '.' + yyyy;

	if (!date_div && this.settings.foaf != null && this.settings.foaf == true) {
		$(".profile_info.profile_info_short").prepend("<div id=\"vk_user_date\" class=\"clear_fix profile_info_row \"><div class=\"label fl_l\">Дата регистрации:</div><div class=\"labeled\">"+date+"</div></div>");
	}
};

Extension.prototype.setUserTime = function(div) {
	if (this.user_info.hidden == 1){
		return;
	}
	
	console.log(this.user_info);
	var
		time_div = document.getElementById("vk_online_time"),

		time 		= new Date(this.user_info.last_seen.time*1000),
		hours 	= time.getHours(),
		minutes = "0" + time.getMinutes(),
		seconds = "0" + time.getSeconds(),
		time 		= hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

	if (!time_div) {
		if ($('#profile_time_lv').find('.profile_mob_onl').length != 0) {
			$('#profile_time_lv').html("Offline<b class=\"mob_onl profile_mob_onl\"></b>");
		}
		else {
			$('#profile_time_lv').html("Offline");
		}
		$(div+', #profile_time_lv').append("<div id=\"vk_online_time\" style=\"float: right;\";>&#160;("+time+")</div>");
	}
};
