(function() {
  var settings = {};

  chrome.storage.local.get('settings', function(result) {
    var data = result.settings;
    console.log(data);

    if (data != null) {
      settings = data;
    }

    $.each(data, function(key, value) {
      console.log(key + " = " + value);
      if (value == true) {
        $('input[type=checkbox][name='+key+']').prop('checked', true);
      } else {
        $('input[type=checkbox][name='+key+']').prop('checked', false);
      }

    });
  });

  save = function(element, key, value) {
    settings[key] = value;

    var saved_message = $("<br /><span>Сохранено!</span>");

    chrome.storage.local.set({'settings': settings}, function() {
      element.after(saved_message
        .hide()
        .fadeIn(600)
        .delay(550)
        .fadeOut(600, function() {
          $(this).remove();
        })
      );
    });
  }

  $('input[type=checkbox]').change(function() {
    save($(this).parent(), $(this).attr('name'), $(this).is(":checked"));
  });

})();
