chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
	var host = 'https://vk.com';

	if (changeInfo.status == 'complete'){
		if (verificationHost(host, tab.url)){
			executeScripts(tabId, [
				{file: "jquery.js"},
				{file: "/assets/js/vk_inject.js"}
			]);
		}
	}
});

function nodeInsertedCallback(event) {
    console.log(event);
};
document.addEventListener('DOMNodeInserted', nodeInsertedCallback);

function verificationHost( host, url ){
	  if( url.indexOf(String(host)) != -1 ){
			return true;
		}

	return false;
}

function executeScripts(tabId, injectDetailsArray)
{
    function createCallback(tabId, injectDetails, innerCallback) {
        return function () {
            chrome.tabs.executeScript(tabId, injectDetails, innerCallback);
        };
    }

    var callback = null;

    for (var i = injectDetailsArray.length - 1; i >= 0; --i)
        callback = createCallback(tabId, injectDetailsArray[i], callback);

    if (callback !== null)
        callback();   // execute outermost function
}
